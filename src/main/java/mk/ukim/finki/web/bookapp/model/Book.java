package mk.ukim.finki.web.bookapp.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Book {

    private String isbn;
    private String title;
    private String genre;
    private int year;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Author> authors;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
    private List<Review> reviews;

    public Book() {
    }

    public Book(String isbn, String title, String genre, int year, List<Author> authors) {
        this.isbn = isbn;
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.authors = authors;

    }

    public Book(String title, String isbn, String genre, int year) {
        this.isbn = isbn;
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.authors = null;

    }
}
